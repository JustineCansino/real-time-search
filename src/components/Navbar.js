import React from 'react';

import Navbar from 'react-bootstrap/Navbar'; 
import { Link} from 'react-router-dom'


export default function NavBar() {



	return(
	 <Navbar bg="dark" expand="lg">
	 	<Navbar.Brand  className="text-light" as={Link} to="/">Real-time search</Navbar.Brand>
	 	<Navbar.Toggle aria-controls="basic-navbar-nav"/>
	 
	 </Navbar>
		)

}