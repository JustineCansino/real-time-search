import React, { useState, useEffect } from 'react';
import Banner from '../components/Banner'
import { Form, Container} from 'react-bootstrap';

export default function Home() {

	const [searchTerm, setSeachTerm] = useState("");

	const [jsonData, setJsonData] = useState([]);

	//Fetching json data inside a useEfect
	useEffect(()=>{
		fetch(`./sample_data_fullstack.json`,
		{
		header: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
				}
		})
		.then(res => res.json())
		.then(data =>{

		setJsonData(data.filter((val)=>{
            if (searchTerm === ""){
                return val
            }else if(val.country.toLowerCase().includes(searchTerm.toLowerCase())){
                return val
            }else if(val.country_code.includes(searchTerm)){
                return val
            }else{
                return false
            }

        }).map((val, key)=>{      //Data display format
                return (      
                <div className="country" key = {key}>  
                <p>{val.country} ({val.country_code})</p>   
                </div>
                )
            }));
	
		

		})


		.catch(err => {
			return err
		});


		

	//Include searchTerm state inside array dependency to avoid infnite response and warnings.
	}, [searchTerm])


	const handleChange = (e) =>{
		setSeachTerm(e.target.value)
	
	}

	


	



	return(
		<>
		<Banner />

		<Container className="homepage">
	
		<Form >
  			<Form.Control id="inputbox" className="typeahead" type="text" placeholder="Search Country Name or Code..." onChange={handleChange}  />
  				
  				
		</Form> <br />

		
		{jsonData}
	
		</Container>
		</>


	);
}