import React from 'react';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import NavBar from './components/Navbar';
import { Route, Switch } from 'react-router-dom';
import Home from './pages/Home';

function App() {
  
  //Used Route, Switch and Router in case of creating additional components and/or pages
  return (
    <Router>
    <NavBar / >
      <Container>
        <Switch>
        <Route exact path="/" component ={Home} />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
